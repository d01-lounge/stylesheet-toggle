(function (d) {
  'use strict';

  var ui,
    styleObjects = [],
    stylesheetUL;

  function initialize() {
    createUI();
    injectStyling();
  }

  function injectStyling() {
	  var link = document.createElement("link");
	  link.setAttribute("rel","stylesheet");
	  link.setAttribute("href","https://lab.littlemissrobot-cdn.com/stylesheet-toggle/stylesheet-toggle-style.css");
	  var head = document.getElementsByTagName("head")[0];
	  head.appendChild(link);
  }

  // Helper Funtctions

  function createUI() {
    var div = d.createElement('div');
    div.className = 'my-ui';
    div.id = "stylesheet-toggle";

    // Text
    div.innerHTML = '<h2>Stylesheets:</h2><ul class=\'stylesheet-list\'></ul><button id=\'add-stylesheet\' class= \'a-button\' type=\'button\'>Add stylesheet</button>';

    var h2 = div.getElementsByTagName('h2')[0];
	h2.onmousedown = startDrag;

    d.querySelector('body').appendChild(div);
    requestStylesheetUrl();
    getCurrentStylesheets();
  }

  function requestStylesheetUrl() {
    var addStylesheetButton = d.querySelector('#add-stylesheet');
    addStylesheetButton.onclick = function (e) {
      var linkToNewStylesheet = window.prompt('Add link to stylesheet');
      addStylesheet(linkToNewStylesheet);
    }
  }

  function addStylesheet(url) {
    var stylesheetTag = 'link';
    stylesheetTag = d.createElement(stylesheetTag);
    stylesheetTag.rel = 'stylesheet';
    stylesheetTag.href = url;
    var newStylesheet = {
      fileName: url,
      visible: true,
      file: stylesheetTag
    };
    styleObjects.push(newStylesheet);
    printCurrentStylesheets();
    addStyling();
  }

  function getCurrentStylesheets() {
    var s = d.querySelectorAll('link[rel=stylesheet]');
    [].slice.call(s).forEach(function (stylesheet) {
      styleObjects.push({ fileName: stylesheet.attributes.href.value, file: stylesheet, visible: true });
    });

    printCurrentStylesheets();
  }

  function printCurrentStylesheets() {
    stylesheetUL = d.querySelector('.my-ui ul.stylesheet-list');
    stylesheetUL.innerHTML = '';
    styleObjects.forEach(function (style, index) {


      var li = d.createElement('li');
      li.attributes.fileName = style.fileName;
      li.attributes.file = style.file;
      li.attributes.index = index;

      var a = d.createElement('a');
      a.innerHTML = style.fileName;
      a.onclick = function (e) {
        style.visible = !style.visible;
        if(style.visible){
        	li.classList.remove('disabled');
			addStyling(style);
		}else{
			removeStyling(style);
			li.classList.add('disabled');
		}
      };

      // Add up button & down buttons
      var up = d.createElement('span'),
        down = d.createElement('span');

      up.className = 'move-stylesheet-up';
      down.className = 'move-stylesheet-down';
      up.innerHTML = '&#8679';
      down.innerHTML = '&#8681';

      up.onclick = reorder;
      down.onclick = reorder;

      li.appendChild(a);
      li.appendChild(up);
      li.appendChild(down);

      stylesheetUL.appendChild(li);
    });
  }

  function reorder(e) {
    e.preventDefault();
    var movingTo = e.target.className === 'move-stylesheet-up' ? 'up' : 'down';

    var parentLI = e.target.parentElement; // wrapping <li>
    var parentUL = parentLI.parentElement; // wrapping <ul>

    if (movingTo === 'up') {
      // we're moving stylesheet up
      if (parentLI.previousElementSibling === null) {
        swapStylesheet(parentLI, parentUL.children[parentUL.children.length - 1], 'after');
      } else {
        swapStylesheet(parentLI.previousSibling, parentLI, 'after');
      }
    } else if (movingTo === 'down') {
      // we're moving stylesheet down
      if (parentLI.nextElementSibling === null) {
        swapStylesheet(parentUL.children[0], parentLI);
      } else {
        swapStylesheet(parentLI, parentLI.nextSibling);
      }
    }
  }

  function swapStylesheet(styleA, styleB, after) {
    var stylesheetA = styleA.attributes.file;
    var stylesheetB = styleB.attributes.file;

    // Make sure to switch order in <head> but also in our ui!
    // Why this after check? There is no such thing as insertAfter, therefore this little hack
    if (after) {
      stylesheetB.parentNode.insertBefore(stylesheetA, stylesheetB.nextSibling);
      styleB.parentNode.insertBefore(styleA, styleB.nextSibling);
    } else {
      stylesheetA.parentNode.insertBefore(stylesheetB, stylesheetA);
      styleA.parentNode.insertBefore(styleB, styleA);
    }

  }

  function removeStyling(style) {
    d.head.removeChild(style.file);
  }

  function addStyling(style) {
    styleObjects.forEach(function (s) {
      if (s.visible) {
        d.head.appendChild(s.file);
      }
    });
  }



	//dragfunctionality
	function startDrag(e){
		window.onmouseup = stopDrag;
		document.addEventListener('mousemove', onDrag);
		var div = document.getElementById('stylesheet-toggle');
		div.ST_offsetTop = e.clientY - div.getBoundingClientRect().top;
		div.ST_offsetLeft = e.clientX - div.getBoundingClientRect().left;
		console.log(div.ST_offsetLeft);

		console.log(e);
	}

	function onDrag(e){
		var div = document.getElementById('stylesheet-toggle');
		div.style.position = 'absolute';
		div.style.top = (e.clientY - div.ST_offsetTop) + 'px';
		div.style.left = (e.clientX - div.ST_offsetLeft) + 'px';
		div.style.bottom = 'auto';
		div.style.right = 'auto';
	}

	function stopDrag(e){
		document.removeEventListener('mousemove', onDrag);
	}



  // initialize

  initialize();

})(window.document);

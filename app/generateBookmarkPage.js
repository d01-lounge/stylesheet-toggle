const fs = require('fs');
const fileName = './dist/index.html';

fs.readFile('./dist/bookmark.js', function (err, data) {
  if (err) {
    console.error('No bookmark.js file found. Please run `npm run bookmark` first!');
    throw err;
  }
  let bookmarkScript = data.toString('utf8');

  writeToHTML(bookmarkScript);
});

function writeToHTML(bookmark) {
  const stream = fs.createWriteStream(fileName);

  stream.once('open', function () {

    const html = `<html>
      <head></head>
      <body>
        <h1>This is a title</h1>
        <a href="${bookmark}" title="Generate bookmark">Toggle Stylesheet</a>
      </body>
      </html>`;

    stream.end(html);
  });
}

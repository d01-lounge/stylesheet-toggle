Stylesheet-toggle
=================

Little bookmark-tool to disable/enable the stylesheets on a webpage or add new stylesheets in an easy way.

This tool can be verry handy to test your local stylesheets on a production-environment.
For example when validating styleguide development.

See the demo in `/demo`.

Usage
-----
Just add the generated javascript snippet as a browser bookmark.
* Make sure nodejs ^6.x.x is installed
* To build the script: `npm run bookmark`


On the wishlist
---------------
See the issues of this repository and ad some with your cool idea's.


----

Build with love at the [D01-lounges](http://lounge.district01.be/) joining the forces of [District01](http://district01.be) and [Little Miss Robot](http://littlemissrobot.com/)

* @mhxbe
* @vdswouter

